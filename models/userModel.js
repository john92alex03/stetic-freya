
const { Schema, model, connection  } = require('mongoose')

const userSchema = new Schema({
  user: String,
  password: String,
})

delete connection.models['users']

const User = model('users', userSchema)

module.exports = User