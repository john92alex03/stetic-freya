
const { Schema, model, connection } = require('mongoose')

const contactSchema = new Schema({
  name: String,
  phone: Number,
  message: String,
  checked: Boolean,
  update: Boolean
})

delete connection.models['Contacts']

const Contact = model('Contacts', contactSchema)

module.exports =  Contact


