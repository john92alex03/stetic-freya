
const { Schema, model, connection } = require('mongoose')


const planSchema = new Schema({
  images: String,
  title: String,
  description: String
})

delete connection.models['Plans']

const Plan = model('Plans', planSchema)

module.exports = Plan