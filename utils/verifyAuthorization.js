const { jwtVerify } = require('jose')

 const verifyUser = async (authorization) => {

   let currentTokenUser = ''
   let user = ''
   
   const keySecret = new TextEncoder().encode(process.env.KEY_SECRET)
   
   let tokenUser = `Bearer ${authorization}`.replace(/"/g, '')
  
   if (tokenUser && tokenUser.toLowerCase().startsWith('bearer')) {
     currentTokenUser = tokenUser.substring(7)
    } else {
      return new Error('invalid token')
    }
  
  try {
    user = await jwtVerify(currentTokenUser, keySecret)

  }catch(err){
    return err.code
  }
      
  
  return user
}

module.exports = { verifyUser }



