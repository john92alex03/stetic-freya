
export default function PanelHome() {
  return (
    <div className = 'h-10/12  w-11/12 xl:w-3/5 md:w-4/5 flex items-center bg-neutral-300 flex-col rounded-xl  shadow-lg mt-10 select-none mb-10'>
      <div className="w-auto px-2">
        <div className="mt-3 sm:mt-7 md:mt-10 px-5 text-center ">
          <h1 className="font-serif font-medium text-5xl md:text-6xl xl:text-6xl" style={{ fontFamily : 'Amatic SC'}}>
            tu estilo de vida
          </h1>
        </div>
        <div className="mt-1 md:mt-3 mb-3 px-1 md:px-2 py-4 content-center antialiased text-base  md:text-xl xl:text-xl 2xl:text-2xl h-auto font-sans  text-center">
          El cuerpo humano tiene muchos beneficios al mantener un peso adecuado. Algunos de estos beneficios incluyen una reducción en el riesgo de enfermedades cardíacas, diabetes tipo 2 y ciertos tipos de cáncer. También puede mejorar la calidad del sueño y reducir el dolor en las articulaciones. Además, mantener un peso adecuado puede mejorar la autoestima y la confianza en uno mismo. Es importante recordar que cada cuerpo es diferente y que el peso adecuado puede variar según la edad, la altura y otros factores individuales.
        </div>
      </div>
    </div>
  )
}


