
import  Link  from 'next/link'


const Planes = () => {
 
  return (
    <div className='bg-blue-200 pb-5'>
      <h2 className='pt-9 px-8 text-5xl md:text-6xl lg:text-7xl text-center' style={ {fontFamily: 'Annie Use Your Telescope'} }>
        planes que mejoraran tu vida
      </h2> 
      <div  className='mb-8'>
        <ul className='grid grid-cols-1  md:grid-cols-2 mt-8 md:mt-16 text-center mb-8 '>
          <li className='h-80 w-80 xl:h-auto xl:w-96 md:96 mb-10 md:mb-0 flex justify-self-center items-center bg-green-200 p-4 rounded-tl-2xl rounded-br-2xl shadow-2xl hover:bg-emerald-100 hover:origin-top-left hover:rotate-6 hover:duration-200 cursor-pointer' >
            <Link href='/Plans/reductor'>
                <div className=''>
                  <h2 className='text-3xl font-serif mb-5 xl:text-4xl'>
                    plan reductor
                  </h2>
                  <div className='capitalize text-base md:text-lg lg:text-xl px-3'>
                    Los masajes reductores pueden ayudar a reducir la celulitis y la grasa localizada en áreas específicas del cuerpo. También pueden mejorar la circulación sanguínea y linfática..
                  </div>
                  <div className='capitalize text-lg md:text-xl px-3 mt-4 font-semibold'>
                    Saber mas..
                  </div>
                </div>
            </Link>
          </li>

          <li className='h-80 w-80 xl:h-96 xl:w-96 md:96 mb-10 md:mb-0 flex justify-self-center items-center bg-green-200 p-4 rounded-tl-2xl rounded-br-2xl shadow-2xl hover:bg-emerald-100
                          hover:origin-top-left hover:rotate-6 hover:duration-200 cursor-pointer'>
            <Link href='/Plans/facial'>
              <div className=''>
                  <h2 className='text-3xl font-serif mb-5 xl:text-4xl'>
                    limpiezas faciales
                  </h2>
                  <div className='capitalize text-base md:text-lg lg:text-xl px-3'>
                    Las limpiezas faciales pueden tener varios beneficios para la piel. Ayudan a eliminar las células muertas de la piel y los poros obstruidos..
                  </div>
                  <div className='capitalize text-lg md:text-xl px-3 mt-4 font-semibold'>
                    Saber mas..
                  </div>
                </div>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Planes;
