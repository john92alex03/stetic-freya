
import { scroller } from 'react-scroll'
import  Link  from 'next/link'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { CiLogout } from 'react-icons/ci'
import { logout } from '../store/reducers/reducersLogin'

export default function Navigator() {

  let stateIsLogin = false
  const dispatch = useDispatch()
  const router = useRouter()
  const [course, setCourse] = useState(null)
  useSelector( (state) => stateIsLogin = state.loginReducer.isLogin)

  let optionScroll = {
    to: "planes" , 
    spy: true , 
    smooth: true , 
    offset: 10, 
    duration: 600, 
    delay: 100
  }  

  function useLogout () {
    dispatch(logout())
    router.push('/')
    localStorage.removeItem('authorization')
  }
  

  useEffect(() => {
    if (router.pathname === '/Plans/[plan]' || router.pathname === '/login' || router.pathname === '/home' ) {
      
      if(course !== null){
        router.push('/')
        setTimeout(()=> scroller.scrollTo(course, optionScroll ), 20 )
        setCourse('')
      }

    } else {
      scroller.scrollTo(course, optionScroll )
      setCourse('')
    }
  }, [course] )
  
  return (
    
    <div>

      <div className="h-24 flex items-center justify-center pb-7 pt-6 mb-4">
        {
          !stateIsLogin ? 
          <Link href='/login' className='z-20 mt-4 2xl:mt-12 mb-8 md:mt-6 absolute right-6 bg-amber-200 h-auto 2xl:h-12 w-16 xl:w-auto px-0 xl:px-4 rounded-lg border-black focus:ring-amber-300 hover:bg-amber-300 text-black font-medium text-sm focus:outline-none text-center py-2 2xl:pt-2 shadow-lg 2xl:text-2xl'>
            Iniciar Sesion
          </Link> : 
          (
            <div className='mt-0 2xl:mt-12 mb-8 md:mt-6 absolute flex right-6'>
              <CiLogout className='z-20 bg-amber-200 h-auto 2xl:h-12 w-auto xl:w-auto px-3 md:px-2 xl:px-4 rounded-lg border-black focus:ring-amber-300 hover:bg-amber-300 text-black font-medium text-sm focus:outline-none text-center py-2 2xl:pt-2 shadow-lg md:text-lg xl:text-lg 2xl:text-xl mr-4' onClick={()=> useLogout() } />
              <Link href='/home' className='z-20 bg-amber-200 h-auto 2xl:h-12 w-auto xl:w-auto px-3 md:px-2 xl:px-4 rounded-lg border-black focus:ring-amber-300 hover:bg-amber-300 text-black font-medium text-sm focus:outline-none text-center py-2 2xl:pt-2 shadow-lg md:text-lg xl:text-lg 2xl:text-xl'>
                Home
              </Link>
            </div>
          )
        }
        
        <h1 className=" text-7xl lg:text-8xl  pt-14 2xl:my-4 md:pt-7 z-10 2xl:text-8xl" style={{ fontFamily : 'Annie Use Your Telescope'}}>
          Stetic <strong className="mr-7">Freya</strong>
        </h1>
        
      </div>

      <div className="flex justify-end w-full box-content h-auto">
        <nav className="grid md:grid-cols-4 sm:grid-cols-1 md:h-28 sm:h-auto 2xl:h-40 bg-gradient-to-r from-amber-200 to-amber-100 divide-x divide-blue-300/25 divide-dashed shadow-md md:w-11/12 sm:w-full w-full rounded-none md:rounded-l-2xl text-5xl sm:text-4xl md:font-semibold lg:text-5xl xl:text-5xl 2xl:text-6xl">
          
          <ul className="flex items-center justify-center ">
            <li className="font-serif hover:scale-105  hover:text-orange-800 cursor-pointer pt-2 sm:pt-2 md:pt-0" style={{ fontFamily: 'Annie Use Your Telescope'}}>
              <Link href='/' >
                Inicio
              </Link>
            </li>
          </ul>

          <ul className="flex items-center justify-center w-auto">
            <li onClick={()=> setCourse('planes')} className="font-serif hover:scale-105 hover:text-orange-800 cursor-pointer pt-2 sm:pt-2 md:pt-0" style={{ fontFamily: 'Annie Use Your Telescope'}}>
              Planes
            </li>
          </ul>

          <ul className="flex items-center justify-center w-auto">
            <li onClick={()=>setCourse('imc')} className="font-serif hover:scale-105 hover:text-orange-800 cursor-pointer pt-2 sm:pt-2 md:pt-0" style={{ fontFamily: 'Annie Use Your Telescope'}}>
              IMC
            </li>
          </ul>

          <ul className="flex items-center justify-center w-auto">
            <li onClick={()=> setCourse('contact')} className="font-serif hover:scale-105 hover:text-orange-800 cursor-pointer pt-2 sm:pt-2 md:pt-0" style={{ fontFamily: 'Annie Use Your Telescope'}}>
              Contactenos
            </li>
          </ul>

        </nav>
      </div>
      
    </div>
  )
}


