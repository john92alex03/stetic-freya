import RedesSocial from "./pure/RedesSocial";
import FormuContact from "./pure/FormuContact";
import { useState, useEffect } from 'react'



const Contactenos = (props) => {

  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [message, setMessage] = useState('')

  const { plan } = props

  useEffect(()=> {

    if(plan === null) {
      return ''
    } else if(plan === 1){
      setMessage('plan 1')
    } else if(plan === 2) {
      setMessage('plan 2')
    }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [message])

  

  return (
    <div className='grid md:grid-cols-2 sm:grid-cols-1 pb-10 h-auto' id='contactos' > 

      <div className="flex items-center justify-center p-5">
        <FormuContact setName={setName} setPhone={setPhone} setMessage={setMessage} name={name} />
      </div>

      <div className="flex items-center justify-center">
        <RedesSocial />
      </div>

    </div>
  );
}

export default Contactenos;
