import React from 'react';
import Image from 'next/image'


const Footer = () => {
  return (
    <div className='rounded-sm shadow h-auto bg-slate-400 flex items-center justify-center'>
      <div className='w-full max-w-screen-xl mx-auto p-4 '>
        <div className='sm:flex sm:items-center sm:justify-between'>

          <div className='flex items-center mb-2 sm:mb-0' style={{ fontFamily: 'Annie Use Your Telescope' }}>
            <Image
              src='/favicons/favicon-96x96.png'
              className='mr-3'
              width={25}
              height={25}
              alt='logo'
            />
            
            <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Stetic freya</span>
          </div>

          <div className='ml-4 text-xs flex flex-wrap items-center font-medium mt-2 2xl:text-lg'>
            <div className='mr-6 hover:underline hover:cursor-pointer md:mr-4'>
              Whatsapp
            </div>
            <div className='mr-6 hover:underline hover:cursor-pointer md:mr-4'>
              Facebook
            </div>
          </div>

          <hr className="border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8 mt-2" />
          <span className="block text-sm text-gray-500 sm:text-center dark:text-gray-400 mt-4">© 2023 <a href="" className="hover:underline">Stetic Freya</a>. All Rights Reserved.</span>
        </div>
      </div>
    </div>
  );
}

export default Footer;
