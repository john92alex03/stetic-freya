import { Formik, Field, Form, ErrorMessage } from "formik";
import axios from 'axios'
import { useState } from 'react'


const FormuContact = (props) => {

  const [ responseInsert, setResponseInsert ] = useState(null)
  const { setName, setPhone, setMessage, name } = props
  const [ isLoading, setIsLoading ] = useState(false)
  

  return (
    <div>
      <div className="text-6xl font-medium title-font mb-2 text-gray-900 ml-3" style={{ fontFamily : 'Amatic SC'}} >
        Contacto
      </div>
      
      <Formik
        
        initialValues={{name: '' , phone: '', message: ''}}
        isSubmitting = {true}
        onSubmit={async (values, action)=> {
          setIsLoading(true)
          setName(values.name)
          setPhone(values.phone)
          setMessage(values.message)

          await axios.post('/api/contact', {
            name : values.name,
            phone : values.phone,
            message : values.message,
            checked: false,
            update: false,
          }).then((response) => {
            setResponseInsert(response.data.result)
          } ).catch((e)=> console.log(e))
          .finally(()=> setIsLoading(false))
        }}

        validate={(values)=> {
          let errors = {}
          const regName = /\d/g
          const regPhone = /\D/g
          let validateReg = values.name.match(regName)
          let validatePhone = values.phone.match(regPhone)

          if(validateReg !== null){
            errors.name = 'caracter incorrecto'
          }
          if(validatePhone !== null){
            errors.phone = 'caracter incorrecto'
          } 
          if (values.message.length <= 0) {
            errors.message = 'campo requerido'
          }
          if (values.name.length <= 0){
            errors.name = 'campo requerido'
          }
          if (values.phone.length <= 0){
            errors.phone = 'campo requerido'
          }
          
          return errors

        }}

        
      >

      {
      ({})=> (
        <Form method="post" >
          <div className="flex flex-wrap m-2">
            <div className="p-2 w-1/2">
              <div className="relative">
                <label htmlFor="name" className="leading-7 text-sm  text-gray-600" style={{ fontFamily: 'Aclonica' }}>Nombre</label>
                <Field type="text" id="name" name="name" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange-200 focus:bg-white focus:ring-1 focus:ring-orange-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out shadow-md" />
                <div className="text-sm text-amber-800 pt-1 font-body" >
                  <ErrorMessage name="name" />
                </div> 
              </div>
            </div>

            <div className="p-2 w-1/2">
              <div className="relative">
                <label htmlFor="phone" className="leading-7 text-sm text-gray-600" style={{ fontFamily: 'Aclonica' }}>Telefono</label>
                <Field type="tel" id="phone" name="phone" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange-200 focus:bg-white focus:ring-1 focus:ring-orange-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out shadow-md" />
                <div className="text-sm text-amber-800 pt-1 font-body">
                  <ErrorMessage name="phone" />
                </div>
              </div>
            </div>

            <div className="p-2 w-full">
              <div className="relative">
                <label htmlFor="message" className="leading-7 text-sm text-gray-600" style={{ fontFamily: 'Aclonica' }}>Mensaje</label>
                <Field as='textarea' id="message" name="message" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange-200 focus:bg-white focus:ring-1 focus:ring-orange-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out shadow-md" />
                <div className="text-sm text-amber-800 pt-1 font-body">
                  <ErrorMessage name="message" />
                </div>
              </div>
            </div>

          </div>

          <div className="ml-4 mb-1 font-sans">
            { !responseInsert ? '' : <p>{responseInsert}</p> }
          </div>

          <button type='submit' style={{ fontFamily: 'Aclonica' }} className="text-base ml-4 shadow-2xl h-10 w-28 rounded-xl text-center py-2 hover:bg-orange-300 hover:cursor-pointer">
              enviar 
              {
              isLoading ? (<svg aria-hidden="true" className="inline w-6 h-6 ml-2 text-gray-200 animate-spin dark:text-gray-600 fill-lime-600" viewBox="0 0 100 101" fill="none">
                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
              </svg> ): ''
              }
          </button>
        </Form>
      )
      }
        
        
        
      </Formik>


    </div>
  );
}

export default FormuContact;
