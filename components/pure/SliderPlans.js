
import { useState } from 'react'
import Image from 'next/image'
import { AiOutlineDoubleLeft, AiOutlineDoubleRight } from 'react-icons/ai'

const SliderPlans = ( props )=> {

  const [currentNum , setCurrentNum] = useState(0)
  const { dbImages, titlePlan } = props

  const handlerCarousel = (method) => {
    if ( method === 'prev') {
      let current = currentNum - 1
      if (current < 0) {
        setCurrentNum(dbImages.length - 1)
      } else {
        setCurrentNum(current)
      }
      
    } if (method === 'next') {
      let current = (currentNum + 1) % dbImages.length
      setCurrentNum(current)
    }
  }

  return (
    <div className='flex flex-col items-center relative'>
        <h1 className='text-6xl md:text-7xl bold font-medium subpixel-antialiased mt-1 mb-1'  style={{ fontFamily : 'Amatic SC'}}> {titlePlan} </h1>
          
        <div className='h-auto grid md:flex-col lg:justify-around sm:h-auto lg:grid-cols-1 mt-8 xl:grid-cols-2 px-8'>
          <div className="w-full h-auto lg:ml-0 lg:w-auto flex justify-center items-center ml-0 xl:ml-14 " >
            <div className="">
              <Image
                src={dbImages[currentNum]?.images || '/imagen_en_blanco.jpg'}
                className="relative ml-0 xl:ml-0  shadow-xl shadow-slate-600/60 border-transparent rounded-xl"
                alt="massage"
                width={450}
                height={450}
              />                   
            </div>
          </div>
          <div className="mt-6 2xl:mt-8 xl:mt-8 sm:mt-5 md:mt-6 lg:mt-6 md:block text-center w-full md:px-2 xl:pr-24">
            <h5 className="text-slate-900 w-auto mb-4 font-medium italic font-serif text-3xl sm:text-3xl md:text-4xl lg:text-4xl xl:text-4xl" >{dbImages[currentNum]?.title}</h5>
            <p className='text-black mt-1 font-serif p-1 text-base md:text-lg lg:text-lg xl:text-xl '>{dbImages[currentNum]?.description}</p>
          </div>
              <span onClick={()=> handlerCarousel('prev') } class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-black/30 dark:bg-gray-800/30 group-hover:bg-black/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-black dark:group-focus:ring-gray-800/70 group-focus:outline-none hover:scale-x-105 hover:bg-amber-300 hover:cursor-pointer absolute left-0 md:left-1 sm:left-4 top-1/2">
                <svg aria-hidden="true" class="w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>
                <span className="sr-only">Previous</span>
              </span>
                
              <span onClick={()=> handlerCarousel('next') } class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-black/30 dark:bg-gray-800/30 group-hover:bg-black/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-black dark:group-focus:ring-gray-800/70 group-focus:outline-none hover:scale-x-105 hover:bg-amber-300 hover:cursor-pointer absolute right-0 md:right-1 sm:right-4 top-1/2">
                <svg aria-hidden="true" class="w-5 h-5 text-black sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                <span className="sr-only">Next</span>
              
        </span>
          
        </div>
    </div>
  )
}

export default SliderPlans



{/* <div id="default-carousel" class="relative w-full" data-carousel="slide">
    <!-- Carousel wrapper -->
    <div class="relative h-56 overflow-hidden rounded-lg md:h-96">
         <!-- Item 1 -->
        <div class="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="/docs/images/carousel/carousel-1.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
        </div>
        <!-- Item 2 -->
        <div class="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="/docs/images/carousel/carousel-2.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
        </div>
        <!-- Item 3 -->
        <div class="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="/docs/images/carousel/carousel-3.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
        </div>
        <!-- Item 4 -->
        <div class="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="/docs/images/carousel/carousel-4.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
        </div>
        <!-- Item 5 -->
        <div class="hidden duration-700 ease-in-out" data-carousel-item>
            <img src="/docs/images/carousel/carousel-5.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
        </div>
    </div>
    <!-- Slider indicators -->
    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
    </div>
    <!-- Slider controls -->
    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev>
        <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>
            <span class="sr-only">Previous</span>
        </span>
    </button>
    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next>
        <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
            <span class="sr-only">Next</span>
        </span>
    </button>
</div> */}



{/* <div className='flex flex-col items-center relative'>
        <h1 className='text-6xl md:text-7xl bold font-medium subpixel-antialiased mt-1 mb-1'  style={{ fontFamily : 'Amatic SC'}}> {titlePlan} </h1>
          
        <div className='h-auto grid md:flex-col lg:justify-around sm:h-auto sm:grid-cols-1 mt-8 lg:grid-cols-2 px-8'>
          <div className="w-11/12 h-auto lg:w-9/12 flex items-center ml-0 md:ml-14 sm:ml-20" >
            <div className="">
              <Image
                src={dbImages[currentNum]?.images}
                className="relative ml-4 md:ml-16  shadow-xl shadow-slate-600/60 border-transparent rounded-xl"
                alt="massage"
                width={500}
                height={500}
              />                   
            </div>
          </div>
          <div className="mt-5 2xl:mt-8 xl:mt-8 sm:mt-5 md:mt-6 lg:mt-0 md:block text-center w-full md:px-5 lg:pr-24">
            <h5 className="text-slate-900 w-11/12 mb-4 font-medium italic font-serif text-2xl sm:text-3xl md:text-4xl lg:text-4xl xl:text-4xl" >{dbImages[currentNum]?.title}</h5>
            <p className='text-black mt-1 font-body leading-loose p-1 text-base sm:text-lg md:text-lg lg:text-lg xl:text-xl '>{dbImages[currentNum]?.description}</p>
          </div>
                <AiOutlineDoubleLeft 
                  className="text-black text-4xl absolute top-1/2 bottom-0 flex items-center justify-center  hover:outline-none hover:no-underline hover:scale-y-110 hover:scale-x-95 hover:cursor-pointer hover:opacity-70 focus:outline-none focus:no-underline left-4 md:left-14 opacity-50"
                  type="button"
                  
                  onClick={()=> handlerCarousel('prev') }
                />
                <AiOutlineDoubleRight 
                  className="text-black text-4xl ml-9 absolute  top-1/2 right-4 md:right-14 flex items-center justify-center  hover:outline-none hover:no-underline hover:scale-y-110 hover:scale-x-95 hover:cursor-pointer hover:opacity-70 focus:outline-none focus:no-underline opacity-50"
                  type="button"
                  onClick={()=> handlerCarousel('next') }
                />
          
        </div>
    </div> */}
