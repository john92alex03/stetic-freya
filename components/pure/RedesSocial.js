import React from 'react';
import { FaInstagram, FaWhatsapp, FaFacebook } from 'react-icons/fa'

const RedesSocial = () => {
  return (
    <div>
      <div className='mb-5 text-center text-4xl ' style={{ fontFamily: 'Annie Use Your Telescope' }}>
        siguenos
      </div>
      <div className='flex'>
        <div className='text-3xl mr-4 cursor-pointer'> 
          <FaInstagram />
        </div>
        <div className='text-3xl mr-4 cursor-pointer'> 
          <FaWhatsapp />
        </div>
        <div className='text-3xl cursor-pointer'> 
          <FaFacebook />
        </div>
      </div>
    </div>
  );
}

export default RedesSocial;
