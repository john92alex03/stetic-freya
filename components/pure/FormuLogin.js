
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useRouter } from 'next/navigation'
import { FaRegEye, FaRegEyeSlash, FaUser } from 'react-icons/fa'
import  { login, logout } from '../../store/reducers/reducersLogin'
import axios from 'axios'

const FormuLogin = () => {
  const [seePassword, setSeePassword] = useState(false)
  const [errorLogin, setErrorLogin] = useState(null)
  const [ isLoading, setIsLoading ] = useState(false)

  const dispatch = useDispatch()

  const { push } = useRouter()
  
  return (
    <div>
      <Formik
        initialValues={initialValues}
        isSubmitting = {true}
        validate={(data)=> {
          let errors = {}
          if(data.user.length <= 0){
            errors.user = 'Campo requerido'        
          }

          if(data.password.length <= 0){
            errors.password = 'Campo requerido'
          }

          return errors
        }}
        onSubmit={async (values, action)=> {
          
          if(values.user === undefined && values.password === undefined){
            setErrorLogin('ingresa tu usuario')
            return 
          }
          
          setIsLoading(true)
          await axios.post('/api/login', {
            user: values.user,
            password : values.password
          }).then((response)=> {
            setErrorLogin(null)
            localStorage.setItem('authorization', JSON.stringify(response.data.token))
            dispatch(login())
            push('/home')
          }).catch((errors)=> {
            localStorage.removeItem('authorization')
            dispatch(logout())
            setErrorLogin(errors.response.data.errors)
          }).finally(()=> setIsLoading(false))
        }}
      >

      {()=> (
        <Form method='post'>
          <div className='ml-3'>
            <label className='text-xl text-gray-500 block font-semibold mb-3 2xl:text-2xl '>Usuario:</label>
            <div className='flex items-center justify-center relative w-full h-14 pt-5 2xl:h-20'>
              <Field placeholder='ingresa tu usuario' type='text' id='user' name='user' className='h-full border rounded-lg px-3 py-2 w-10/12 mb-5  text-xl focus:ring-amber-300 focus:border-amber-300 2xl:text-2xl ' />  

              <div className='mb-5 relative text-lg text-stone-600 right-7 2xl:right-11 2xl:text-2xl' >
                <FaUser />
              </div>

            </div>

            <div className=' flex justify-center items-center w-full mb-2 '>
              <div className='w-10/12 text-red-600 font-semibold 2xl:text-xl' >
                <ErrorMessage name='user' />
              </div>
            </div>

           

            <label className='text-xl text-gray-500 block font-semibold mb-3 2xl:text-2xl '>Contraseña:</label>
            <div className=' flex items-center justify-center relative w-full h-14 pt-5 2xl:h-20'>
              <Field placeholder='ingresa tu contraseña' type={seePassword ? 'text' : 'password'} id='password' name='password' className='h-full border rounded-lg px-3 py-2 w-10/12 mb-5  text-xl focus:ring-amber-300 focus:border-amber-300 2xl:text-2xl ' />  

              <div className='mb-5 relative text-lg text-stone-600 right-8 2xl:right-11  2xl:text-2xl hover:cursor-pointer'  onClick={()=> setSeePassword(!seePassword) } >
                {seePassword ? <FaRegEye /> : <FaRegEyeSlash /> }
              </div>

            </div>

            <div className=' flex justify-center items-center w-full mb-2 ' >
              <div className='w-10/12 font-semibold 2xl:text-xl text-red-600 '>
                <ErrorMessage name='password' />
              </div>
            </div>

            <div className='flex items-center justify-center relative w-full flex-col'>
              <button type='submit' className='mt-3 transition duration-300 bg-amber-300 hover:bg-amber-400 focus:bg-amber-500 focus:shadow-sm focus:ring-4 focus:ring-opacity-50 focus:ring-amber-300 text-black w-10/12 py-1 rounded-lg shadow-sm hover:shadow-md font-semibold inline-block text-center 2xl:h-16'>
                <span className='inlinae-block 2xl:text-2xl '>
                  Iniciar Sesion
                </span>
                {
                  isLoading && (
                    <svg aria-hidden="true" className="inline w-6 h-6 2xl:h-10 2xl:w-10 2xl:mb-1 ml-8 text-gray-200 animate-spin dark:text-gray-600 fill-lime-600" viewBox="0 0 100 101" fill="none">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                  )
                }                          
              </button>
              <div className='mt-5 font-semibold 2xl:text-2xl'>
                {
                  errorLogin && ( <h4> {errorLogin} </h4> )
                }
              </div>
            </div>
          </div>

        </Form>
      )}

      </Formik>
    </div>
  );
}

const initialValues = {
  user: '',
  password: '',
}

export default FormuLogin;
