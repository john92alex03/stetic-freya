
import { useState } from 'react'
import { ErrorMessage, Formik, Form, Field } from 'formik'
import { GoArrowLeft } from 'react-icons/go'


const ImcComponent = () => {

  const [imc, setImc] = useState(null)
  const [imcResult , setImcResult ] = useState('low')

  const getImc = ( kgPerson, altPerson ) => {
  
    const imcPerson = kgPerson / (altPerson/100)**2
  
    const imcResult = imcPerson.toFixed(2)
  
    if( imcResult < 18.50 ) {
      setImcResult('low')
      return { imcTotal : 'peso inferior a lo normal', imc : imcResult, description: 'Esto puede tener consecuencias negativas para la salud, como mayor riesgo de infecciones, anemia o osteoporosis. Para aumentar el IMC y alcanzar un peso saludable, se recomienda seguir una dieta equilibrada y hacer ejercicio físico moderado. Un masaje ayuda a estimular la circulación sanguínea, eliminar toxinas, tonificar los músculos y eliminar la grasa acumulada. Así, se consigue reducir el volumen corporal y mejorar el aspecto de la piel. Si quieres mejorar tu IMC y sentirte mejor contigo mismo, prueba un masaje y disfruta de sus beneficios' }
    } else if ( imcResult >=18.50 && imcResult <= 24.90) {
      setImcResult('proper')
      return { imcTotal : 'peso adecuado', imc : imcResult, description: 'Un IMC entre 18,5 y 24,9 indica que el peso es adecuado para la estatura. Esto se asocia con un menor riesgo de enfermedades cardiovasculares, diabetes o hipertensión. Para mantener el IMC y el peso ideal, se aconseja seguir una alimentación variada y hacer actividad física regular. Un masaje reductor es un buen complemento para mejorar la silueta y la salud. Con el masaje se consigue reducir el volumen corporal y mejorar el aspecto de la piel. Si quieres cuidar tu IMC y sentirte más atractivo, prueba un masaje reductor y disfruta de sus beneficios'  }
    } else if ( imcResult >= 25.00 && imcResult <= 30.00 ) {
      setImcResult('high')
      return { imcTotal : 'peso superior al normal', imc : imcResult, description: 'Un IMC superior a 25 indica que el peso es superior al normal para la estatura. Esto puede tener consecuencias negativas para la salud, como mayor riesgo de obesidad, colesterol o artrosis. Para reducir el IMC y alcanzar un peso saludable, se recomienda seguir una dieta baja en calorías y hacer ejercicio físico intenso. Un masaje reductor ayuda a acelerar el proceso de pérdida de peso y a moldear la figura, con el masaje reductor se consigue reducir el volumen corporal y mejorar el aspecto de la piel. Si quieres bajar tu IMC y sentirte más ligero, prueba un masaje reductor y disfruta de sus beneficios'  }
    } else if ( imcResult > 30.00 ) {
      setImcResult('obesity')
      return { imcTotal : 'obesidad', imc : imcResult, description: 'Un IMC superior a 30 indica que hay obesidad, lo que significa que el peso es mucho mayor al normal para la estatura. Esto puede tener consecuencias graves para la salud, como mayor riesgo de infarto, apnea del sueño o cáncer. Para alcanzar un peso saludable, se recomienda seguir una dieta muy baja en calorías y hacer ejercicio físico. un masaje reductor puede ser un buen apoyo para combatir la obesidad y a mejorar la autoestima. El masaje reductor es una técnica que aplica presión sobre las zonas con mayor acumulación de grasa para favorecer su eliminación. Si quieres bajar tu IMC y sentirte más saludable, prueba un masaje reductor y disfruta de sus beneficios'  }
    }
  }


  return (
    <>
    <div className='bg-orange-100 xl:h-96 h-auto z-10' ></div>
    <div className='flex py-10 items-center justify-center flex-col bg-amber-200 w-full z-0' >
      <h1 className='text-5xl md:text-6xl xl:text-7xl mb-9' style={ {fontFamily: 'Annie Use Your Telescope'} } >
        calcula tu IMC
      </h1>
      <Formik
        initialValues={{ kg: 0 , alt : 0  }}
        onSubmit = {
          (values) => {
            
            const imc = getImc(values.kg, values.alt)
            setImc(imc)

          }
        }
        validate={(values)=> {
          let errors = {}

          if(values.kg < 40 ) {
            errors.kg = 'debe ser mas de 40 kg'
          } 
          if(values.alt < 150) {
            errors.alt = 'debe ser mas de 150 cm'
          }

          return errors
        }}
      >

      {({ errors,  })=> (
        <div className='flex w-full'>          
          { !imc ? ( 
            <Form className='w-full flex flex-col text-center items-center' method='post' >
              <div className="mb-2 flex flex-col justify-center items-center w-9/12 xl:w-8/12">
                
                <label htmlFor="kg" className="text-sm md:text-lg xl:text-xl mb-3" style={{ fontFamily: 'Aclonica' }} >escribe tu peso (en kg)</label>
                <Field type='number' id='kg' name='kg' className="bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange-200 focus:bg-white focus:ring-1 focus:ring-orange-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out h-5/6 xl:h-11 w-11/12  md:h-full md:w-7/12 lg:w-6/12 shadow-lg xl:text-xl lg:text-lg text-center font-semibold"  />
                
              </div>

              <div className='mb-2 text-center w-full xl:text-xl text-amber-800'>
                <ErrorMessage name='kg' />
              </div>

              <div className="flex justify-center items-center flex-col w-9/12 xl:w-8/12">
                <label htmlFor="alt" className=" text-sm md:text-lg xl:text-xl mb-3" style={{ fontFamily: 'Aclonica' }} >escribe tu altura (en cm)</label>
                <Field type='number' id='alt' name='alt' className="bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange-200 focus:bg-white focus:ring-1 focus:ring-orange-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out h-5/6 md:h-full xl:h-11 w-11/12  md:w-7/12 lg:w-6/12 shadow-lg xl:text-xl lg:text-lg text-center font-semibold"  />
              </div>

              <div className='mt-2 text-center w-full xl:text-xl text-amber-800'>
                <ErrorMessage name='alt' />
              </div>

              <div className='flex justify-center w-full'>
                <button type="submit" className='bg-orange-300 w-28 h-9 rounded-md shadow-md font-serif text-xl p-1 mt-4 hover:scale-105' >calcular</button>
              </div>
            </Form> 
            ) :
              <div className='w-full flex justify-center '>
                <div className='flex flex-col bg-orange-100	 h-fit text-base lg:text-lg w-11/12 lg:w-8/12 xl:w-6/12 2xl:w-5/12 text-center px-0 lg:px-3 py-6 shadow-lg rounded-md' style= {{ fontFamily: 'Aclonica' }} >
                      <div className='w-full flex '>
                        <div className='ml-1 md:ml-2'>
                          <GoArrowLeft className='cursor-pointer text-2xl ' onClick={()=> setImc(null)}  />
                        </div>
                        <div className='mb-2 w-auto ml-4 text-lg md:text-xl'>
                          tu resultado es: <span className='ml-2 text-xl md:text-2xl' >{imc.imc}</span>
                        </div> 
                      </div>
                      <div className='p-4 text-sm md:text-base xl:text-lg'>
                        <span className='text-base md:text-xl font-semibold'>{imc.imcTotal}</span>: {imc?.description}
                      </div>
                    </div> 
              </div>
            }
        </div>
      )}
      </Formik>
    </div>
    </>
  );
}

export default ImcComponent;



