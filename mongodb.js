const  { connect, connection, set } = require('mongoose')

  async function connectDb () {
        
      set('strictQuery', false)
      let conn = {
        isConnected : false
      }

      if(conn.isConnected) return
    
      const db = await connect(process.env.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
    
      conn.isConnected = db.connections[0].readyState
      // connection.useDb('dataBase-app-freya')
  }

  connection.on('connected', ()=> {
    console.log('conexion establecida')
  })

  connection.on('error', (err)=> {
    console.log(err)
  })
  
module.exports = connectDb




 

  







// import { MongoClient } from 'mongodb'

// const url = process.env.MONGODB_URL
// const options = {
//   useUnifiedTopology: true,
// }


// let mongoClient
// let connection

// if (!process.env.MONGODB_URL) {
//   console.log('por favor ingresa el archivo con los datos .env')
// }

// if(process.env.NODE_ENV === 'development') {

//   console.log(process.env)

//   if(!global._mongoClientPromise){

//     mongoClient = new MongoClient(url, options)
//     global._mongoClientPromise = mongoClient.connect()
//   }

//   connection = global._mongoClientPromise
// } else {
//   mongoClient = new MongoClient(url, options)
//   connection = mongoClient.connect()
// }


// export default connection

