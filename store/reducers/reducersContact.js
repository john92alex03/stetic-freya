
import { createSlice } from '@reduxjs/toolkit'

const initialValues = []

export const reducersContact = createSlice({
  name:'contact',
  initialState: initialValues,
  reducers: {
    getMessagesContact: (state, action)=> {
      state.push(action.payload)
    }, 
    deleteMessagesContact : (state, action)=> {
      state = []
    },
    updateCheckedMessage: (state, action)=> {
      console.log(action.payload[0])
    }
    
  }
})

export const { getMessagesContact, deleteMessagesContact, updateCheckedMessage } = reducersContact.actions


export default reducersContact.reducer