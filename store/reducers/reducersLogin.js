


// import { LOGIN, LOGOUT } from '../actions/loginActions'
import { createSlice } from '@reduxjs/toolkit'

const initialValues = {
   isLogin: null,
   
}

export const reducerLogin = createSlice({
  name: 'updateLogin',
  initialState: initialValues,
  reducers: {
    login: (state, action) => {
      state.isLogin = true
    },
    logout: (state, action)=> {
      state.isLogin = false
    }
  }
})

export const { login, logout } = reducerLogin.actions

export default reducerLogin.reducer

