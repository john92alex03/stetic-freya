
import { combineReducers  } from '@reduxjs/toolkit'

import { loginUser } from './reducersLogin'

import { reducersContact } from './reducersContact'

export const reducerApp = combineReducers({
  loginUser,
  reducersContact
})