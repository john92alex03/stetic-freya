
import { configureStore } from '@reduxjs/toolkit'
import loginReducer from '../reducers/reducersLogin'
import messageContactReducer from '../reducers/reducersContact'


const store = configureStore({
  reducer : {
    loginReducer,
    messageContactReducer
  }
})

export default store

