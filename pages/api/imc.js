
import axios from 'axios'


export default function imcCalculator(req,res) {

 let kg = req.body.kg
 let altura = req.body.altura
 let estatura = null
 let imc = null
 let result = null

  if( req.method === 'POST') {
    estatura = altura / 100
    imc = kg / estatura ** 2
        
  } else {
    return res.state(404).json({ result : 'ingresa tus datos' })
  }

  return res.status(200).json({ imcResult : 'se reviso su imc' }) 
}