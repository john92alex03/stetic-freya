const connectDb  = require('/mongodb')

const Contact = require('/models/contactModel')

const { verifyUser } = require('../../utils/verifyAuthorization')

connectDb()

export default async function handler(req, res) {

  const verify = await verifyUser(req.headers.authorization)

  if (req.method === 'POST') {
    const newContact = new Contact({
      name : req.body.name,
      phone : req.body.phone,
      message : req.body.message,
      checked: req.body.checked
    })

    newContact.save().then( response => {
      if(response === newContact){
        return res.status(200).json({result : 'mensaje registrado correctamente' })
      } 
    } ).catch(e=> res.status(406).json({ result : e}) )
    
  }
  
  if(req.method === 'GET'){
      if(verify?.payload){

      try {
        const data = await Contact.find({})
        return res.status(200).json(data)
      } catch (error) {
        return res.status(502).send('invalid token')
      }

    } else {
      
      res.status(502).send('invalid token')
    } 
  }

  

 
}