const connectDb = require('/mongodb')

const Contact = require('/models/contactModel')

const { useRouter } = require('next/router')

const { verifyUser } = require('/utils/verifyAuthorization')

connectDb()

export default async function messageContact(req, res) {

  const verify = await verifyUser(req.headers.authorization)


  if(verify?.payload){
    
    if(req.method === 'DELETE') {
      Contact.deleteOne({ _id: req.body.userId}).then((response)=>
      { 
        res.status(200).send('mensaje eliminado')
      })
    }
    
  }

}