const connectDb = require('../../../mongodb')

const Contact = require('../../../models/contactModel')

const { useRouter } = require('next/router')

const { verifyUser } = require('../../../utils/verifyAuthorization')

connectDb()

export default async function updateMessageContact(req, res) {

  verifyUser(req.headers.authorization)

  if(req.method === 'PUT') {
    await Contact.updateOne({ _id: req.query.id }, { checked: req.body.checked })
    res.status(200).send('mensaje modificado')
  }
}