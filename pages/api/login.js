const connectDb = require('/mongodb')

const User = require('/models/userModel')

const { SignJWT } = require('jose')

const { serialize } = require('cookie')

connectDb()

async function login (req, res) {
    let datos = []
    const alg = 'HS256'

    if(req.method === 'POST'){
        
        const data = await User.find({ user: req.body.user})
        
        if(data[0]?.user === undefined || data[0]?.password === undefined) {
            return res.status(502).json({ errors: 'el usuario o contraseña son incorrectos' })
        } 

        if(data[0]?.user === req.body.user && data[0]?.password === req.body.password) {

            const secret = new TextEncoder().encode(process.env.KEY_SECRET)
            
            const userKey = await new SignJWT({ user: data[0].id}).setProtectedHeader({ alg }).setExpirationTime('12h').sign(secret)

            res.setHeader('Set-Cookie', 
                serialize('token', userKey, {
                    httpOnly: true,
                    maxAge: 60 * 60,
                    sameSite: 'strict',
                    secure: process.env.NODE_ENV !== 'development',
                    path: '/',

                })
            )
            
            return res.status(200).send({ token: userKey })

        } else {
            return res.status(502).json({ errors: 'el usuario o contraseña son incorrectos' })
        }

    }
    
}

module.exports = login