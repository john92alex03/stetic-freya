
import PanelHome from '../components/PanelHome'
import Planes from '../components/Planes'
import Footer from '../components/Footer'
import Contactenos from '../components/Contactenos'
import ImcComponent from '../components/Imc'


export default function Home() {  

  return (
    <>
      <div className='min-h-screen min-w-screen' >

        <div className='flex justify-center mt-0  md:mt-10 h-auto'>
          <PanelHome />
        </div>
        
        <div id='plans' name='planes' className=''>
          <Planes />
        </div>

        <div name='imc' className='xl:h-3/5 h-auto flex justify-center' id='imc'>
          <ImcComponent />
        </div>

        <div name='contact' className='h-auto bg-orange-50'>
          <Contactenos />
        </div>

        <div >
          <Footer />
        </div>  
                
      </div>
    </>
  )
}
