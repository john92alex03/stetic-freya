
import '../styles/globals.css'
import Layout from '../layout/Layout'
import store from '../store/config/configReducers'
import { Provider } from 'react-redux'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp
