

import { useState, useEffect } from 'react'
import Footer from '../../components/Footer'
import Contactenos from '../../components/Contactenos'
import SliderPlans from '../../components/pure/SliderPlans'
import axios from 'axios'
import { useRouter } from 'next/router'

import { AiOutlineCheck } from 'react-icons/ai'


const packagePlansReductor = [
  
    {
      title : 'Paquete 1',
      description : 'este paquete consiste de 20 sesiones en los cuales se trabaja maderoterapia, mesoterapia y masajes manuales',
      paquetes : [
        '9 sesiones masaje manual',
        '7 sesiones de maderoterapia',
        '4 sesiones de mesoterapia'
      ]
    },
    {
      title : 'Paquete 2',
      description : 'este paquete consiste de 30 sesiones en los cuales se trabaja maderoterapia, mesoterapia y masajes manuales',
      paquetes : [
        '12 sesiones masaje manual',
        '9 sesiones de maderoterapia',
        '7 sesiones de mesoterapia'
      ]
    }
  
]

const packagePlansLimpiezas = [
  
    {
      title : 'Paquete 1',
      description : 'este paquete consiste de 5 sesiones en los cuales se trabaja limpieza facial superficial y profunda',
      paquetes : [
        '2 limpieza facial superficial',
        '3 limpieza facial profunda'
      ]
    },
    {
      title : 'Paquete 2',
      description : 'este paquete consiste de 10 sesiones en los cuales se trabaja limpieza facial superficial y profunda',
      paquetes : [
        '6 limpieza facial superficial',
        '4 limpieza facial profunda'
      ]
    }
  
]

const Plans = () => {

const [dbImages, setDbImages] = useState([])
const [plan, setPlan] = useState(0)
const [positionPlan, setPositionPlan] = useState(-1)
const [titlePlan , setTitlePlan] = useState('')
const [ packagePlan , setPackagePlan ] = useState([])
const router = useRouter()



useEffect(()=> {
  
  if(router.query.plan === 'reductor'){
    setPositionPlan(0)
    setTitlePlan('Planes reductores')
    setPackagePlan(packagePlansReductor)
  }
  
  if(router.query.plan === 'facial') {
    setPositionPlan(1)
    setTitlePlan('Limpiezas faciales')
    setPackagePlan(packagePlansLimpiezas)
  }
    
    axios.get('/api/plansDb',{
      headers: {
        'plan' : positionPlan
      }
    }
    ).then((response)=> {
          setDbImages(response.data.plans.plan)
        })
        .catch((err)=> {
          if(err.message == 'Cannot read properties of undefined (reading "plan")'){
            window.localStorage.removeItem('authorization')
          }
        })
  },[positionPlan, ])


  return (
    <div className='flex flex-col mt-6 w-full ' >
      <div className='mb-7 lg:mb-16 h-auto w-full flex justify-center sm:h-4/5' >
        <div className='w-11/12'>
          <SliderPlans dbImages={dbImages} titlePlan={titlePlan} />
        </div>
      </div>

        
      <div className='h-auto grid justify-items-center w-full items-center mb-10 lg:mt-5 px-0 lg:px-8 md:mt-24 mt-8'>

          { packagePlan && (
        <div className=' grid justify-items-center md:grid-cols-2 sm:grid-cols-1 w-12/12 lg:w-4/5 mb-10 sm:mb-10 md:mb-0'>
          {
            packagePlan.map((plan)=> {
              return (
                <div key={plan?.title} className='border-slate-800 h-auto  w-9/12 md:w-10/12 lg:w-10/12 xl:w-8/12 2xl:w-7/12 rounded-md bg-gradient-to-r from-neutral-300 to-neutral-200 hover:to-neutral-300 shadow-xl hover:scale-105 hover:shadow-2xl py-5 px-6 my-1'>
                <h1 className='text-center italic text-2xl font-medium mb-2'>{plan?.title}</h1>
                <p className='text-center font-sans text-base md:text-lg '> {plan?.description} </p>
                  <div>
                    <ul className='text-center text-base md:text-lg mt-3 text-zinc-700'>                                           

                      {
                        plan?.paquetes.map( (element) => {
                          return (
                            <li key={element} className='mb-1 flex w-full'> <AiOutlineCheck className='mr-3 mt-1' /> {element}</li>
                          )
                        })
                      }                  
                      
                    </ul>
                  </div>
                </div>
            )
            })
          }
          
        </div>
          ) 
          }
          
      </div>
      

      <div className='h-1/2 bg-amber-100'>
        <Contactenos plan={plan} />
      </div>

      <div className=''>
        <Footer />
      </div>  

    </div> 
  );
}



export default Plans;