
import { useEffect, useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter, } from 'next/navigation'
import axios from 'axios'
import { logout } from '../store/reducers/reducersLogin'
import { FiSquare, FiCheckSquare, FiTrash2 } from 'react-icons/fi'


const Home = ()=> {
  const { push  } = useRouter()
  const dispatch   = useDispatch()
  const isLogin = useSelector((state)=> state.loginReducer.isLogin)
  const [ data, setData ] = useState([])
  const [ check, setCheck ] = useState(false)
  const [ deleteMessageState , setDeleteMessageState ] = useState(false)
  const [ showButton, setShowButton ] = useState(false)

  useEffect( ()=> {
    
    getMessages()
    setDeleteMessageState(false)

  },[deleteMessageState])

  function getMessages(){

    axios.get('/api/contact',{

      headers : {
        authorization: window.localStorage.getItem('authorization')
      }

    }).then((res)=> {

      setData(res.data)

    }).catch((err)=> {

      window.localStorage.removeItem('authorization')
      logout()

    } )
  }

  function deleteMessage(id){
    axios.delete('/api/messageContact', {
      headers : {
        authorization: window.localStorage.getItem('authorization')
      },
      data: {
        userId : id
      }
    }).then((response)=> console.log(response))
      .catch((e)=> console.log(e))
  }
  

  function updateMessage(i) {
    data[i].checked = !data[i].checked
  }

  function updateShowButton(i){
    data[i].update = true
    setShowButton(true)
  }


  function submitButtonUpdate (i, id) {
    data[i].update = false
    const updateChecked = data[i].checked
    setShowButton(false)
    axios.put(`/api/messageContact/${id}`, { checked: updateChecked }).then((response)=> console.log(response.data))
  }

  useEffect(()=> {
    if(isLogin === false){
      setTimeout(()=> { push('/') }, 500)
    }
  }, [isLogin])

  return (
    <div className='flex justify-center '>
      <div className=' container h-auto mt-10'>
        <h1 className='font-mono text-3xl mb-7 text-center'>mensajes recibidos</h1>
        
        <ul className='w-full flex justify-center items-center flex-col' >
          {data.map((message, index) => (
              <li key={message.id}  className={ message.checked ? 'mb-4 w-11/12 flex justify-between p-6 bg-slate-200 border border-gray-200 rounded-lg shadow hover:bg-gray-100 ' : 'mb-4 w-11/12 flex justify-between p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100' } >
                
                  <div>
                    <h5 className='mb-2 text-2xl xl:text-3xl font-bold tracking-tight text-gray-900 '>{message.name}</h5>
                    <h5 className='mb-2 text-xl xl:text-2xl font-bold tracking-tight text-gray-900 '>{message.phone}</h5>
                    <p className=' text-lg xl:text-xl font-normal text-gray-700'>{ message.message }</p>
                  </div>

                  <div className='w-10 h-24 flex items-center justify-center flex-col relative'>
                    <div className='text-2xl hover:cursor-pointer mb-3' onClick={()=> { 
                              updateMessage(index)
                              setCheck(!check)
                              updateShowButton(index)
                          }} >
                      { message.checked ? <FiCheckSquare /> : <FiSquare /> }
                    </div>

                    <div className='text-2xl hover:cursor-pointer' onClick={()=> { 
                              deleteMessage(message._id)  
                              setDeleteMessageState(true)  
                          }} >
                      <FiTrash2 />
                    </div>

                    <div className='text-2xl hover:cursor-pointer mt-1  top-2 relative' >
                      { message.update && <button className='text-black  bg-amber-300 hover:bg-amber-500 focus:ring-4 focus:ring-amber-200 font-semibold rounded-lg text-sm px-4 py-2 mr-3 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800' onClick={()=> 
                        {
                          submitButtonUpdate(index, message._id)
                        } } 

                        >aplicar</button> }

                    </div>

                  </div>
                
              </li>
              )).reverse()
            }
          </ul>
      
      </div>
    </div>
  )
}

export default Home
