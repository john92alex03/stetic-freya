
import Footer from '../components/Footer'
import FormuLogin from '../components/pure/FormuLogin'

const Login = () => {
  return (
    <>
      <div className='w-full 2xl:h-full 2xl:my-12 flex flex-col items-center justify-center h-auto py-1'>
        

        <div className=' bg-gray-200 h-96 my-8  w-11/12 md:w-7/12 lg:w-5/12 2xl:w-4/12 flex flex-col shadow rounded-lg divide-y divide-gray-200 2xl:py-10 sm:h-auto'>
          
          <h1 className="mt-4 text-center font-bold text-4xl md:text-5xl  title-font text-gray-900 2xl:text-6xl" style={{ fontFamily : 'Amatic SC'}} >
            Iniciar Sesion
          </h1>
          
          <div className='md:px-5 px-2 md:py-7 py-4'>

            <FormuLogin />

          </div>
          
        </div>

      </div>

      <div className='inline-block w-full'>
        <Footer />
      </div>
      
    </>

  );
}

export default Login;
